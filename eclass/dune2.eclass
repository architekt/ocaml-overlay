# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: dune2.eclass
# @MAINTAINER:
# rkitover@gmail.com
# @AUTHOR:
# Rafael Kitover <rkitover@gmail.com>
# @SUPPORTED_EAPIS: 5 6 7
# @BLURB: Provides functions for installing dune packages.
# @DESCRIPTION:
# Provides dependencies on dune and ocaml and default src_compile, src_test and
# src_install for dune-based packages.

# @ECLASS-VARIABLE: DUNE_PKG_NAME
# @DESCRIPTION:
# Sets the actual dune package name, if different from gentoo package name.
# Set before inheriting the eclass.

case ${EAPI:-0} in
	5|6|7) ;;
	*) die "${ECLASS}: EAPI ${EAPI} not supported" ;;
esac

EXPORT_FUNCTIONS src_compile src_test src_install

RDEPEND=">=dev-lang/ocaml-4:=[ocamlopt?]"
DEPEND="${RDEPEND}
	dev-ml/dune"

dune2_src_compile() {
	local pkg="${1:-${DUNE_PKG_NAME:-${PN}}}"

	dune build --root . --only-packages "${pkg}" --profile release --default-target @install || die
}

dune2_src_test() {
	dune runtest || die
}

# @FUNCTION: dune-install
# @USAGE: <list of packages>
# @DESCRIPTION:
# Installs the dune packages given as arguments. For each "${pkg}" element in
# that list, "${pkg}.install" must be readable from "${PWD}/_build/default"
dune-install() {
	local pkg
	for pkg ; do
		dune install \
			--prefix="${ED%/}/usr" \
			--libdir="${D%/}$(ocamlc -where)" \
			"${pkg}" || die
	done
}

dune2_src_install() {
	local pkg="${1:-${DUNE_PKG_NAME:-${PN}}}"

	dune-install "${pkg}"

	# No docs
	if ! use doc ; then
		if [ -d "${ED%/}/usr/doc/${pkg}" ] ; then
			rm -rf "${ED%/}/usr/doc" || die
		fi
	fi

	# Move docs to the appropriate place.
	if [ -d "${ED%/}/usr/doc/${pkg}" ] ; then
		mkdir -p "${ED%/}/usr/share/doc/${PF}/" || die
		mv "${ED%/}/usr/doc/${pkg}/"* "${ED%/}/usr/share/doc/${PF}/" || die
		rm -rf "${ED%/}/usr/doc" || die
	fi
}
