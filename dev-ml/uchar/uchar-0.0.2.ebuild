# Copyright 2019-2020 Moteel Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit findlib

DESCRIPTION="Uchar compatibility library"
HOMEPAGE="https://github.com/ocaml/uchar"
SRC_URI="https://github.com/ocaml/uchar/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2-with-linking-exception"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~x86"
IUSE="ocamlopt"
RESTRICT="test"

RDEPEND="dev-lang/ocaml:="
DEPEND="${RDEPEND}
	dev-ml/ocamlbuild"

src_compile() {
	ocaml pkg/build.ml \
	"native=$(usex ocamlopt true false)" \
	"native-dynlink=$(usex ocamlopt true false)" || die
}

src_test() {
	ocamlbuild -X src -use-ocamlfind -pkg uchar test/testpkg.native || die
}

src_install() {
	findlib_src_preinst
	mv _build/pkg/META{.empty,} || die
	ocamlfind install ${PN} _build/pkg/META || die
	dodoc README.md CHANGES.md
}
