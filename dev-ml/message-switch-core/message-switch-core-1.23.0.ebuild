# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DUNE_PKG_NAME="${PN}"

inherit dune2

MY_PN="message-switch"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="A simple store-and-forward message switch"
HOMEPAGE="https://github.com/xapi-project/message-switch/"
SRC_URI="https://github.com/xapi-project/${MY_PN}/archive/v${PV}.tar.gz -> ${MY_P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="doc ocamlopt"

RDEPEND="" ## TODO FROM SCRATCH
DEPEND="${RDEPEND}"

S="${WORKDIR}/${MY_P}"

src_configure() {
	./configure \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--destdir="${D}"
}
