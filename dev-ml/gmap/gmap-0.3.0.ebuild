# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Heterogenous maps over a GADT"
HOMEPAGE="https://github.com/hannesm/gmap/"
SRC_URI="https://github.com/hannesm/gmap/archive/0.3.0.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND=""
DEPEND="${RDEPEND}"

