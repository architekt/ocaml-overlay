# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 dune

DESCRIPTION="RFC3986 URI parsing library for OCaml"
HOMEPAGE="https://github.com/mirage/ocaml-uri/"

EGIT_REPO_URI="https://github.com/mirage/ocaml-${PN}.git"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
IUSE="ocamlopt"

RDEPEND="dev-lang/ocaml:=[ocamlopt?]"
DEPEND="${RDEPEND}"

