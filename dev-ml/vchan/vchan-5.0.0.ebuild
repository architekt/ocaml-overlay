# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Pure OCaml implementation of the 'vchan' shared-memory communication protocol"
HOMEPAGE="https://github.com/mirage/ocaml-vchan/"
SRC_URI="https://github.com/mirage/ocaml-vchan/archive/v5.0.0.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/lwt:=
	dev-ml/cstruct:=
	dev-ml/ppx_sexp_conv:=
	dev-ml/ppx_cstruct:=
	dev-ml/io-page:=
	dev-ml/mirage-flow:=
	dev-ml/xenstore:=
	dev-ml/xenstore_transport:=
	dev-ml/sexplib:=
	dev-ml/cmdliner:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-vchan-5.0.0"

dune_src_compile() {
        dune build -p vchan -j1 || die
}
