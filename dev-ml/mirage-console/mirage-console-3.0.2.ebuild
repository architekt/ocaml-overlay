# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Portable console handling for Mirage applications"
HOMEPAGE="https://github.com/mirage/mirage-console/"
SRC_URI="https://github.com/mirage/mirage-console/archive/v3.0.2.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/mirage-device:=
	dev-ml/mirage-flow:=
	dev-ml/lwt:=
	dev-ml/cstruct:="
DEPEND="${RDEPEND}"

