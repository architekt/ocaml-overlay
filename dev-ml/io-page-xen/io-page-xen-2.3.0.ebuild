# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Support for efficient handling of I/O memory pages on Xen"
HOMEPAGE="https://github.com/mirage/io-page/"
SRC_URI="https://github.com/mirage/io-page/archive/v2.3.0.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="=dev-ml/io-page-${PV}:=
	dev-ml/cstruct:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/io-page-2.3.0"
