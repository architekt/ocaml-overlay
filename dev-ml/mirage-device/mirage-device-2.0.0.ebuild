# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Types for basic abstract devices"
HOMEPAGE="https://github.com/mirage/mirage-device/"
SRC_URI="https://github.com/mirage/mirage-device/archive/v2.0.0.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/fmt:=
	dev-ml/lwt:="
DEPEND="${RDEPEND}"

