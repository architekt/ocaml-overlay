# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Convert from random byte vectors (Cstruct.t) to random native numbers"
HOMEPAGE="https://github.com/hannesm/randomconv/"
SRC_URI="https://github.com/hannesm/randomconv/archive/v0.1.3.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/cstruct:="
DEPEND="${RDEPEND}"

