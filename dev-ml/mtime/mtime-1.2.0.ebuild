# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit findlib eutils

DESCRIPTION="Monotonic wall-clock time for OCaml"
HOMEPAGE="https://github.com/dbuenzli/mtime/"
SRC_URI="https://github.com/dbuenzli/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~x86"
IUSE="ocamlopt javascript test"
RESTRICT="!test? ( test )"

RDEPEND="dev-lang/ocaml:=[ocamlopt]
	javascript? ( dev-ml/js_of_ocaml:= )"
DEPEND="${RDEPEND}
	dev-ml/ocamlbuild
	dev-ml/findlib
	dev-ml/topkg"

src_compile() {
	ocaml pkg/pkg.ml build \
		--with-js_of_ocaml $(usex javascript true false) \
		--tests $(usex test true false) \
		|| die
}

src_test() {
	ocaml pkg/pkg.ml test || die
}

src_install() {
	findlib_src_preinst

	local mtimelibs="$(echo \
		_build/src/${PN}*.cm{x,xa,xs,t,ti} _build/src/${PN}*.a _build/src/${PN}*.{ml,mli} _build/src/${PN}*.cm{a,i})"
	ocamlfind install ${PN} _build/pkg/META ${mtimelibs} || die

	local clockoslibs="$(echo \
		_build/src-os/${PN}*.cm{x,xa,xs,t,ti} _build/src-os/${PN}*.a _build/src-os/${PN}*.{ml,mli} _build/src-os/${PN}*.cm{a,i})"
	ocamlfind install ${PN}/os _build/pkg/META _build/src-os/libmtime_clock_stubs.a ${clockoslibs} || die
	rm "${D}"/usr/$(get_libdir)/ocaml/${PN}/os/META

	if use javascript; then
		local clockjslibs="$(echo \
		_build/src-jsoo/${PN}*.cm{x,xa,xs,t,ti} _build/src-jsoo/${PN}*.a _build/src-jsoo/${PN}*.{ml,mli} _build/src-jsoo/${PN}*.cm{a,i})"
		ocamlfind install ${PN}/jsoo _build/pkg/META ${clockjslibs}  || die
		rm "${D}"/usr/$(get_libdir)/ocaml/${PN}/jsoo/META
	fi
}
