# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

MY_PN="xen-api"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="The Xapi Project's XenAPI Server"
HOMEPAGE="https://github.com/xapi-project/xen-api/"
SRC_URI="https://github.com/xapi-project/${MY_PN}/archive/v${PV}.tar.gz -> ${MY_P}.tar.gz"

LICENSE="GPL-2.1"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-lang/ocaml:=[ocamlopt?]"
DEPEND="${RDEPEND}"

S="${WORKDIR}/${MY_P}"

src_configure() {
	./configure \
		--bindir=/opt/xensource/bin \
		--cluster-stack-root=/usr/libexec/xapi/cluster-stack \
		--disable-warn-error \
		--docdir=/usr/share/xapi/doc \
		--etcdir=/etc/xensource \
		--extensiondir=/etc/xapi.d/extensions \
		--hooksdir=/etc/xapi.d \
		--inventory=/etc/xensource-inventory \
		--libexecdir=/opt/xensource/libexec \
		--optdir=/opt/xensource \
		--plugindir=/etc/xapi.d/plugins \
		--sbindir=/opt/xensource/bin \
		--scriptsdir=/etc/xensource/scripts \
		--sharedir=/opt/xensource \
		--udevdir=/etc/udev \
		--varpatchdir=/var/patch \
		--webdir=/opt/xensource/www \
		--xapiconf=/etc/xapi.conf
}
