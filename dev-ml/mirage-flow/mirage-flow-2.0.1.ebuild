# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Flow implementations"
HOMEPAGE="https://github.com/mirage/mirage-flow/"
SRC_URI="https://github.com/mirage/mirage-flow/archive/v2.0.1.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/cstruct:=
	dev-ml/fmt:=
	dev-ml/lwt:="
DEPEND="${RDEPEND}"

