# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Xen core platform libraries"
HOMEPAGE="https://github.com/mirage/mirage-xen/"
SRC_URI="https://github.com/mirage/mirage-xen/archive/v5.0.0.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/cstruct:=
	dev-ml/lwt:=
	dev-ml/shared-memory-ring-lwt:=
	dev-ml/xenstore:=
	dev-ml/xen-evtchn:=
	dev-ml/lwt-dllist:=
	dev-ml/mirage-profile:=
	dev-ml/mirage-xen-ocaml:=
	dev-ml/io-page-xen:=
	dev-ml/mirage-xen-minios:=
	dev-ml/mirage-runtime:=
	dev-ml/logs:=
	dev-ml/fmt:="
DEPEND="${RDEPEND}"

