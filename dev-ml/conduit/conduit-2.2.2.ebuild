# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

MY_PN="ocaml-conduit"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="Dereference URIs into communication channels for Async or Lwt"
HOMEPAGE="https://github.com/mirage/ocaml-conduit/"
SRC_URI="https://github.com/mirage/${MY_PN}/archive/v${PV}.tar.gz -> ${MY_P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/ppx_sexp_conv:=
	dev-ml/sexplib:=
	dev-ml/astring:=
	dev-ml/uri:=
	dev-ml/logs:=
	dev-ml/ipaddr:=
	dev-ml/ipaddr-sexp:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/${MY_P}"

dune_src_compile() {
        dune build -p ${PN} ${MAKEOPTS} || die
}
