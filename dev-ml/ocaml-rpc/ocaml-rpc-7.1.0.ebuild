# Copyright 1999-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

#DUNE_PKG_NAME="fileutils"

inherit dune

DESCRIPTION=""
HOMEPAGE="https://github.com/mirage/ocaml-rpc"
SRC_URI="https://github.com/mirage/ocaml-rpc/archive/v7.1.0.tar.gz -> ${PN}-${PV}.tar.gz"

LICENSE="LGPL-2.1-with-linking-exception"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="ocamlopt"

DEPEND=""
#>=dev-ml/ounit-2.0.0
#>=dev-ml/stdlib-shims-0.2.0"

#DOCS=( "README.md" "CHANGES.md" "LICENSE.txt" )

#S="${WORKDIR}/${DUNE_PKG_NAME}-v${PV}"
