# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Pure DNS resolver API"
HOMEPAGE="https://github.com/mirage/ocaml-dns/"
SRC_URI="https://github.com/mirage/ocaml-dns/archive/v4.6.1.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/cstruct:=
	dev-ml/fmt:=
	dev-ml/logs:=
	dev-ml/dns:=
	dev-ml/rresult:=
	dev-ml/randomconv:=
	dev-ml/domain-name:=
	dev-ml/ipaddr:=
	dev-ml/lwt:=
	dev-ml/mirage-stack:=
	dev-ml/mirage-random:=
	dev-ml/mirage-time:=
	dev-ml/mirage-clock:=
	dev-ml/mtime:=
	dev-ml/mirage-crypto-rng:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-dns-4.6.1"

dune_src_compile() {
        dune build -p dns-client -j1 || die
}
