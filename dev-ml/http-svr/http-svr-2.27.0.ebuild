# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DUNE_PKG_NAME="${PN}"

inherit dune2

MY_PN="xen-api-libs-transitional"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="Further transitional libraries required by xapi"
HOMEPAGE="https://github.com/xapi-project/xen-api-libs-transitional/"
SRC_URI="https://github.com/xapi-project/${MY_PN}/archive/v${PV}.tar.gz -> ${MY_P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="doc ocamlopt"

RDEPEND="=dev-ml/stunnel-${PV}" ## TODO FROM SCRATCH
DEPEND="${RDEPEND}"

S="${WORKDIR}/${MY_P}"
