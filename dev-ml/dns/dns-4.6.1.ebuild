# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="An opinionated Domain Name System (DNS) library"
HOMEPAGE="https://github.com/mirage/ocaml-dns/"
SRC_URI="https://github.com/mirage/ocaml-dns/archive/v4.6.1.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/rresult:=
	dev-ml/astring:=
	dev-ml/fmt:=
	dev-ml/logs:=
	dev-ml/ptime:=
	dev-ml/domain-name:=
	dev-ml/gmap:=
	dev-ml/cstruct:=
	dev-ml/ipaddr:=
	dev-ml/lru:=
	dev-ml/duration:=
	dev-ml/metrics:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-dns-4.6.1"

dune_src_compile() {
        dune build -p dns -j1 || die
}
