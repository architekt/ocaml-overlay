# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DUNE_PKG_NAME="xapi-xenopsd"

inherit dune2

DESCRIPTION="A single-host domain/VM manager for the Xen hypervisor"
HOMEPAGE="https://github.com/xapi-project/xenopsd"
SRC_URI="https://github.com/xapi-project/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="doc ocamlopt"

RDEPEND="dev-ml/base:=
	dev-ml/astring:=
	dev-ml/cohttp:=
	dev-ml/fmt:=
	dev-ml/forkexec:=
	dev-ml/ppx_deriving_rpc:=
	dev-ml/ppx_sexp_conv:=
	dev-ml/re:=
	dev-ml/result:=
	dev-ml/rpclib:=
	dev-ml/rresult:=
	dev-ml/sexplib:=
	dev-ml/sexplib0:=
	dev-ml/uri:=
	dev-ml/uuidm:=
	dev-ml/uutf:=
	dev-ml/xapi-backtrace:=
	dev-ml/xapi-idl:=
	dev-ml/xapi-stdext-date:=
	dev-ml/xapi-stdext-pervasives:=
	dev-ml/xapi-stdext-threads:=
	dev-ml/xapi-stdext-unix:=
	dev-ml/xenctrl:=
	dev-ml/xmlm:=" ## TODO FROM SCRATCH
DEPEND="${RDEPEND}"

src_configure() {
	./configure \
		--bindir=/usr/bin \
		--sbindir=/usr/sbin \
		--etcdir=/etc \
		--libexecdir=/usr/$(get_libdir)/xenopsd \
		--mandir=/usr/share/man \
		--optdir=/opt/xensource/libexec \
		--qemu_wrapper_dir=/usr/$(get_libdir)/xenopsd \
		--scriptsdir=/usr/$(get_libdir)/xenopsd/scripts
}

