# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=5

OASIS_BUILD_TESTS=0

inherit oasis

MY_PN="ocaml-inotify"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="OCaml bindings to inotify"
HOMEPAGE="https://github.com/whitequark/ocaml-inotify"
SRC_URI="https://github.com/whitequark/${MY_PN}/archive/v${PV}.tar.gz -> ${MY_P}.tar.gz"

LICENSE="LGPL-2.1-with-linking-exception"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~x86"
IUSE="debug doc lwt +ocamlopt profiling test"

RDEPEND="dev-lang/ocaml:=[ocamlopt?]
	lwt? ( dev-ml/lwt:=[ocamlopt?] )"
DEPEND="${RDEPEND}
	dev-ml/findlib[ocamlopt?]
	test? ( dev-ml/ocaml-fileutils:=[ocamlopt?] )"

S="${WORKDIR}/${MY_P}"

src_configure() {
	oasis_configure_opts="$(use_enable doc docs) $(use_enable lwt) $(use_enable profiling profile)"
	oasis_src_configure
}
