# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit findlib

DESCRIPTION="Pretty-printing library for OCaml"
HOMEPAGE="https://github.com/ocaml-community/easy-format/"
SRC_URI="https://github.com/ocaml-community/${PN}/archive/${PV}.tar.gz -> ${PN}-${PV}.tar.gz"

SLOT="0/${PV}"
LICENSE="BSD"
KEYWORDS="~amd64"

IUSE="ocamlopt"

RDEPEND="dev-lang/ocaml:=[ocamlopt?]"
DEPEND="${RDEPEND}"

src_compile() {
        dune build -p easy-format -j1 
}

src_install() {
        findlib_src_preinst
        local nativelibs="$(echo _build/install/default/lib/${PN}/META _build/install/default/lib/${PN}/easy_format.cm{x,xa,xs,ti} _build/install/default/lib/${PN}/easy_format.a)"
        ocamlfind install ${PN} _build/install/default/lib/${PN}/easy_format.mli _build/install/default/lib/${PN}/easy_format.cm{a,i} ${nativelibs} || die
        dodoc CHANGES.md README.md
}
