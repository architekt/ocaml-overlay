# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

inherit findlib

DESCRIPTION="Declarative events and signals for OCaml"
HOMEPAGE="https://github.com/dbuenzli/react"
SRC_URI="https://github.com/dbuenzli/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~arm ~ppc ~x86"
IUSE="test"
RESTRICT="!test? ( test )"

RDEPEND=""
DEPEND="${RDEPEND}
	dev-ml/findlib
	>=dev-ml/topkg-0.9"

src_compile() {
	ocaml pkg/pkg.ml build \
		--tests $(usex test 'true' 'false') || die
}

src_test() {
	ocaml pkg/pkg.ml test || die
}

src_install() {
        findlib_src_preinst
        local nativelibs="$(echo _build/src/${PN}*.cm{x,xa,xs,t,ti} _build/src/${PN}*.a)"
        ocamlfind install ${PN} _build/pkg/META _build/src/${PN}*.ml{,i} _build/src/${PN}*.cm{a,i} ${nativelibs} || die
        dodoc CHANGES.md README.md
}
