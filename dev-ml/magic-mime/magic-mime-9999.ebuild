# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 dune

DESCRIPTION="Map filenames to common MIME types"
HOMEPAGE="https://github.com/mirage/ocaml-magic-mime/"

EGIT_REPO_URI="https://github.com/mirage/ocaml-magic-mime.git"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-lang/ocaml:=[ocamlopt?]"
DEPEND="${RDEPEND}"

