# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Network protocol module type definitions"
HOMEPAGE="https://github.com/mirage/mirage-protocols/"
SRC_URI="https://github.com/mirage/mirage-protocols/archive/v4.0.1.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/mirage-device:=
	dev-ml/mirage-flow:=
	dev-ml/fmt:=
	dev-ml/duration:=
	dev-ml/lwt:=
	dev-ml/ipaddr:=
	dev-ml/macaddr:="
DEPEND="${RDEPEND}"

