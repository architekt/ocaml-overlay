# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit findlib

DESCRIPTION="OCaml Format pretty-printer combinators"
HOMEPAGE="https://github.com/dbuenzli/fmt/"
SRC_URI="https://erratique.ch/software/${PN}/releases/${PN}-${PV}.tbz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-lang/ocaml:=
	dev-ml/base:=
	dev-ml/cmdliner:="
DEPEND="${RDEPEND}
	dev-ml/ocamlbuild
	dev-ml/findlib
	dev-ml/topkg"

src_compile() {
	ocaml pkg/pkg.ml build || die
}

src_install() {
	findlib_src_preinst
	local nativelibs="$(echo _build/src/${PN}*.cm{x,xa,xs,t,ti} _build/src/${PN}*.a _build/src/${PN}*.{ml,mli} _build/src/${PN}*.cm{a,i})"
	ocamlfind install ${PN} _build/pkg/META ${nativelibs} || die
	dodoc CHANGES.md README.md
}
