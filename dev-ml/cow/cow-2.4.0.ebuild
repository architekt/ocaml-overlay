# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Set of parsers and syntax extensions to let you manipulate HTML, CSS, XML, JSON and Markdown"
HOMEPAGE="https://github.com/mirage/ocaml-cow/tags/"
SRC_URI="https://github.com/mirage/ocaml-cow/archive/v2.4.0.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-lang/ocaml:=[ocamlopt?]"
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-${PN}-${PV}"
