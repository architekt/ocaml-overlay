# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Functional Priority Search Queues"
HOMEPAGE="https://github.com/pqwy/psq/"
SRC_URI="https://github.com/pqwy/psq/archive/v0.2.0.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/seq:="
DEPEND="${RDEPEND}"

