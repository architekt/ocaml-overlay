# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Collect profiling information"
HOMEPAGE="https://github.com/mirage/mirage-profile/"
SRC_URI="https://github.com/mirage/mirage-profile/archive/v0.9.0.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/cstruct:=
	dev-ml/ppx_cstruct:="
DEPEND="${RDEPEND}"

dune_src_compile() {
        dune build -p mirage-profile -j1 || die
}
