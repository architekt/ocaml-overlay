# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Minimal POSIX compatability layer for the MirageOS Xen backend"
HOMEPAGE="https://github.com/mirage/mirage-platform/blob/master/mirage-xen-posix.opam/"
SRC_URI="https://github.com/mirage/mirage-platform/archive/v3.3.2.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-lang/ocaml:=[ocamlopt?]"
DEPEND="${RDEPEND}"

S="${WORKDIR}/mirage-platform-3.3.2"
