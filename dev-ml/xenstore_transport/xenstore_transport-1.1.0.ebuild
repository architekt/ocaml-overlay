# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="low-level libraries for connecting to a xenstore service on a xen host"
HOMEPAGE="https://github.com/xapi-project/ocaml-xenstore-clients/"
SRC_URI="https://github.com/xapi-project/ocaml-xenstore-clients/archive/v1.1.0.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/lwt:=
	dev-ml/xenstore:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-xenstore-clients-1.1.0"
