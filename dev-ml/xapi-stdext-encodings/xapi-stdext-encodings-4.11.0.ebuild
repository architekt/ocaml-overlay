# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune2

MY_PN="stdext"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="A deprecated collection of utility functions - Encodings module"
HOMEPAGE="https://github.com/xapi-project/stdext/"
SRC_URI="https://github.com/xapi-project/${MY_PN}/archive/v${PV}.tar.gz -> ${MY_P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="doc ocamlopt"

RDEPEND=""
DEPEND="${RDEPEND}"

S="${WORKDIR}/${MY_P}"
