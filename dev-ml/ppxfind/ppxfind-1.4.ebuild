# Copyright 2019-2020 Moteel Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Ocamlfind ppx tool"
HOMEPAGE="https://github.com/jeremiedimino/ppxfind/"
SRC_URI="https://github.com/jeremiedimino/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/ocaml-migrate-parsetree:="
DEPEND="${RDEPEND}"

