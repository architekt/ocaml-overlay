# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Bindings which allow Unix.file_descrs to be sent and received over Unix domain sockets"
HOMEPAGE="https://github.com/xapi-project/ocaml-fd-send-recv/"
SRC_URI="https://github.com/xapi-project/ocaml-${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND=""
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-${PN}-${PV}"
