# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Dereference URIs into communication channels for Async or Lwt"
HOMEPAGE="https://github.com/mirage/ocaml-conduit/"
SRC_URI="https://github.com/mirage/ocaml-conduit/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/ppx_sexp_conv:=
	dev-ml/sexplib:=
	dev-ml/cstruct:=
	dev-ml/mirage-stack:=
	dev-ml/mirage-clock:=
	dev-ml/mirage-flow:=
	dev-ml/mirage-flow-combinators:=
	dev-ml/mirage-random:=
	dev-ml/mirage-time:=
	dev-ml/dns-client:=
	dev-ml/conduit-lwt:=
	dev-ml/vchan:=
	dev-ml/xenstore:=
	dev-ml/tls:=
	dev-ml/tls-mirage:=
	dev-ml/ipaddr:=
	dev-ml/ipaddr-sexp:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-conduit-${PV}"

dune_src_compile() {
        dune build -p conduit-mirage -j1 || die
}
