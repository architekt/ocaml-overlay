# Copyright 2019-2020 Moteel Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Generation of runtime types from type declarations"
HOMEPAGE="https://github.com/janestreet/ppx_typerep_conv/"
SRC_URI="https://github.com/janestreet/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/base:=
	dev-ml/typerep:=
	dev-ml/ppxlib:="
DEPEND="${RDEPEND}"

#PATCHES=( "${FILESDIR}"/upgrade-to-ppxlib-0.14.0.patch )
