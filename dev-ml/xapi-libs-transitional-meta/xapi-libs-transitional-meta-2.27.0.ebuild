# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Further transitional libraries required by xapi"
HOMEPAGE="https://github.com/xapi-project/xen-api-libs-transitional/"

LICENSE="metapackage"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE=""

RDEPEND="=dev-ml/gzip-${PV}
	=dev-ml/http-svr-${PV}
	=dev-ml/pciutil-${PV}
	=dev-ml/resources-${PV}
	=dev-ml/sexpr-${PV}
	=dev-ml/stunnel-${PV}
	=dev-ml/uuid-${PV}
	=dev-ml/xapi-compression-${PV}
	=dev-ml/xml-light2-${PV}
	=dev-ml/zstd-${PV}"
