# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Helper functions to preserve and transport exception backtraces"
HOMEPAGE="https://github.com/xapi-project/backtrace/"
SRC_URI="https://github.com/xapi-project/backtrace/archive/v0.7.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/base:=
	dev-ml/ppx_deriving_rpc:=
	dev-ml/ppx_sexp_conv:=
	dev-ml/rpclib:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/backtrace-${PV}"
