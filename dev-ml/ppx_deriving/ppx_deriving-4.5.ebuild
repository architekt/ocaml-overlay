# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Type-driven code generation for OCaml"
HOMEPAGE="https://github.com/ocaml-ppx/ppx_deriving"
SRC_URI="https://github.com/ocaml-ppx/ppx_deriving/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt test"
RESTRICT="!test? ( test )"

RDEPEND="dev-ml/ocaml-migrate-parsetree:=
	dev-ml/ppx_derivers:=
	dev-ml/ppx_tools:=
	dev-ml/result:="
DEPEND="${RDEPEND}
	dev-ml/cppo
	dev-ml/ppxfind
	test? ( dev-ml/ounit )"
