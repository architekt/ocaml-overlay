# Copyright 2019-2020 Moteel Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Convert OCaml parsetrees between different major versions"
HOMEPAGE="https://github.com/ocaml-ppx/ocaml-migrate-parsetree/"
SRC_URI="https://github.com/ocaml-ppx/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1-with-linking-exception"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/result:=
	dev-ml/ppx_derivers:="
DEPEND="${RDEPEND}"
