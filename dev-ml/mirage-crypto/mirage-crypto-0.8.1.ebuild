# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Cryptographic primitives"
HOMEPAGE="https://github.com/mirage/mirage-crypto/"
SRC_URI="https://github.com/mirage/mirage-crypto/archive/v0.8.1.tar.gz -> mirage-crypto-${PV}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/cstruct:=
	dev-ml/eqaf:=
	dev-ml/duration:=
	dev-ml/zarith:=
	dev-ml/mirage-clock:=
	dev-ml/mirage-time:="
DEPEND="${RDEPEND}"

