# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

inherit findlib

DESCRIPTION="Functional reactive programming with incremental changes in data structures"
HOMEPAGE="https://github.com/ocsigen/reactiveData"
SRC_URI="https://github.com/ocsigen/reactiveData/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-3-with-linking-exception"
SLOT="0/${PV}"
KEYWORDS="~amd64"
IUSE="ocamlopt"

RDEPEND="dev-ml/react:="
DEPEND="${RDEPEND}
	dev-ml/findlib
	dev-ml/ocamlbuild"

src_compile() {
	ocaml pkg/build.ml \
		native=$(usex ocamlopt true false) \
		native-dynlink=$(usex ocamlopt true false) \
		|| die
}

src_install() {
	findlib_src_preinst
	local nativelibs="$(echo _build/src/${PN}*.cm{x,xa,xs,t,ti} _build/src/${PN}*.a)"
	ocamlfind install ${PN} _build/pkg/META _build/src/${PN}*.ml{,i} _build/src/${PN}*.cm{a,i} ${nativelibs} || die
	dodoc CHANGES README.md
}
