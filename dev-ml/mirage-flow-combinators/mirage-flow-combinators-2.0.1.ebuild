# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Flow implementations and combinators for MirageOS specialized to lwt"
HOMEPAGE="https://github.com/mirage/mirage-flow/"
SRC_URI="https://github.com/mirage/mirage-flow/archive/v2.0.1.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/fmt:=
	dev-ml/lwt:=
	dev-ml/logs:=
	dev-ml/cstruct:=
	dev-ml/mirage-clock:=
	=dev-ml/mirage-flow-${PV}:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/mirage-flow-2.0.1"
