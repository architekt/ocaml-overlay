# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Compiler from OCaml to Javascript"
HOMEPAGE="https://github.com/ocsigen/js_of_ocaml/"
SRC_URI="https://github.com/ocsigen/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/ocaml-migrate-parsetree:=
	dev-ml/ppx_tools_versioned:=
	dev-ml/uchar:="
	#js_of_ocaml-compiler
DEPEND="${RDEPEND}"

