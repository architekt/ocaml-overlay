# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Embed typed ASN.1 grammars in OCaml"
HOMEPAGE="https://github.com/mirleft/ocaml-asn1-combinators/"
SRC_URI="https://github.com/mirleft/ocaml-asn1-combinators/archive/v0.2.2.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/cstruct:=
	dev-ml/zarith:=
	dev-ml/bigarray-compat:=
	dev-ml/stdlib-shims:=
	dev-ml/ptime:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-asn1-combinators-0.2.2"
