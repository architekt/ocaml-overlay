# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Primitives for Elliptic Curve Cryptography taken from Project Everest"
HOMEPAGE="https://github.com/mirage/hacl/"
SRC_URI="https://github.com/mirage/hacl/archive/v0.1.1.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/cstruct:=
	dev-ml/eqaf:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/hacl-0.1.1"
