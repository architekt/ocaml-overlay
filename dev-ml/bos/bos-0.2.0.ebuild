# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit findlib

DESCRIPTION="Basic OS interaction for OCaml"
HOMEPAGE="https://github.com/dbuenzli/bos/"
SRC_URI="https://github.com/dbuenzli/bos/archive/v0.2.0.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt test"
RESTRICT="!test? ( test )"

RDEPEND="dev-lang/ocaml:=
	dev-ml/fpath:="
DEPEND="${RDEPEND}
	dev-ml/ocamlbuild
	dev-ml/findlib
	dev-ml/topkg
	test? ( dev-ml/mtime )"

src_compile() {
	ocaml pkg/pkg.ml build --tests $(usex test true false) || die
}

src_test() {
	ocaml pkg/pkg.ml test || die
}

src_install() {
	findlib_src_preinst
	local nativelibs="$(echo _build/src/${PN}*.cm{x,xa,xs,t,ti} _build/src/${PN}*.a _build/src/${PN}*.{ml,mli} _build/src/${PN}*.cm{a,i})"
	ocamlfind install ${PN} _build/pkg/META ${nativelibs} || die
	dodoc CHANGES.md README.md
}
