# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit findlib

DESCRIPTION="Alternative String module for OCaml"
HOMEPAGE="https://github.com/dbuenzli/astring/"
SRC_URI="https://github.com/dbuenzli/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-lang/ocaml:="
DEPEND="${RDEPEND}
	dev-ml/ocamlbuild
	dev-ml/findlib
	dev-ml/topkg"

src_compile() {
	ocaml pkg/pkg.ml build --pkg-name ${PN} || die
}

src_install() {
	findlib_src_preinst
	local nativelibs="$(echo _build/src/${PN}*.cm{x,xa,xs,t,ti} _build/src/${PN}*.a _build/src/${PN}*.{ml,mli} _build/src/${PN}*.cm{a,i})"
	ocamlfind install ${PN} _build/pkg/META ${nativelibs} || die
	dodoc CHANGES.md README.md
}
