# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune2

MY_PN="ocaml-qmp"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="OCaml implementation of a Qemu Message Protocol (QMP) client"
HOMEPAGE="https://github.com/xapi-project/ocaml-qmp"
SRC_URI="https://github.com/xapi-project/${MY_PN}/archive/v${PV}.tar.gz -> ${MY_P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="doc ocamlopt"

RDEPEND="" ## TODO FROM SCRATCH
DEPEND="${RDEPEND}"

S="${WORKDIR}/${MY_P}"
