# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Access C-like structures directly from OCaml"
HOMEPAGE="https://github.com/mirage/ocaml-cstruct/"
SRC_URI="https://github.com/mirage/ocaml-cstruct/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="=dev-ml/cstruct-${PV}:=
	dev-ml/ppx_tools_versioned:=
	dev-ml/ocaml-migrate-parsetree:=
	dev-ml/sexplib:=
	dev-ml/stdlib-shims:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-cstruct-${PV}"
