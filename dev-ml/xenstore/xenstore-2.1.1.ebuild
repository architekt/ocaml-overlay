# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="A Xenstore protocol implementation in pure OCaml"
HOMEPAGE="https://github.com/mirage/ocaml-xenstore/"
SRC_URI="https://github.com/mirage/ocaml-xenstore/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/cstruct:=
	dev-ml/ppx_cstruct:=
	dev-ml/lwt:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-xenstore-${PV}"
