# Copyright 2019-2020 Moteel Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Base library and tools for ppx rewriters"
HOMEPAGE="https://github.com/ocaml-ppx/ppxlib/"
SRC_URI="https://github.com/ocaml-ppx/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/base:=
	dev-ml/ocaml-compiler-libs:=
	dev-ml/ocaml-migrate-parsetree:=
	dev-ml/ppx_derivers:=
	dev-ml/stdio:="
DEPEND="${RDEPEND}"
