# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Simple public-key cryptography"
HOMEPAGE="https://github.com/mirage/mirage-crypto/"
SRC_URI="https://github.com/mirage/mirage-crypto/archive/v0.8.1.tar.gz -> mirage-crypto-${PV}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/duration:=
	dev-ml/cstruct:=
	dev-ml/logs:=
	=dev-ml/mirage-crypto-${PV}:=
	dev-ml/mtime:=
	dev-ml/lwt:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/mirage-crypto-${PV}"
