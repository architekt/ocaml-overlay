# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="X509 (RFC5280) handling in OCaml"
HOMEPAGE="https://github.com/mirleft/ocaml-x509/"
SRC_URI="https://github.com/mirleft/ocaml-x509/archive/v0.11.2.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/cstruct:=
	dev-ml/asn1-combinators:=
	dev-ml/ptime:=
	dev-ml/base64:=
	dev-ml/mirage-crypto:=
	dev-ml/mirage-crypto-pk:=
	dev-ml/rresult:=
	dev-ml/fmt:=
	dev-ml/gmap:=
	dev-ml/domain-name:=
	dev-ml/logs:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-x509-0.11.2"
