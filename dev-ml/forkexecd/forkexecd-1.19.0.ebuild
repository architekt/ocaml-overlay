# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

#DUNE_PKG_NAME="${PN}"
#DUNE_PKG_NAME="${PN}"

inherit dune

#MY_PN=""
#MY_P="${MY_PN}-${PV}"

DESCRIPTION="Service for starting and managing other services"
HOMEPAGE="https://github.com/xapi-project/forkexecd/"
SRC_URI="https://github.com/xapi-project/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE=""
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="doc ocamlopt"

RDEPEND="" ## TODO FROM SCRATCH
DEPEND="${RDEPEND}"

#S="${WORKDIR}/${MY_P}"

src_prepare() {
	eapply_user
	sed -i -e 's|systemd||' src/dune || die
	sed -i -e 's|"systemd" {>= "1.2"}||' xapi-forkexecd.opam || die
}
