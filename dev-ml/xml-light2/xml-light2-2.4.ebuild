# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit eutils

DESCRIPTION="Minimal Xml parser and printer for OCaml"
HOMEPAGE="http://tech.motion-twin.com/xmllight.html"
SRC_URI="https://github.com/ncannasse/xml-light/archive/${PV}.tar.gz -> ${PN}-${PV}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0/${PV}"
KEYWORDS="amd64 ~arm ~arm64 ~ppc x86"
IUSE="doc ocamlopt"

RDEPEND="dev-lang/ocaml:=[ocamlopt?]"
DEPEND="app-arch/unzip
	${RDEPEND}"

S="${WORKDIR}/xml-light-${PV}"

src_compile() {
	emake -j1
	if use ocamlopt; then
		emake -j1 opt
	fi
	if use doc;then
		emake doc
	fi
}

src_install() {
	dodir /usr/$(get_libdir)/ocaml/${PN}
	emake INSTALLDIR="${D}"/usr/$(get_libdir)/ocaml/${PN} install

	if use ocamlopt; then
		emake INSTALLDIR="${D}"/usr/$(get_libdir)/ocaml/${PN} installopt
		echo 'archive(native)="xml-light.cmxa"' >> "${D}"/usr/$(get_libdir)/ocaml/${PN}/META
	fi
	dodoc README
	if use doc; then
		emake doc
		dohtml doc/*
	fi
}
