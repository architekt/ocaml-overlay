# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="A library for manipulation of IP (and MAC) address representations"
HOMEPAGE="https://github.com/mirage/ocaml-ipaddr/"
SRC_URI="https://github.com/mirage/ocaml-ipaddr/archive/v${PV}.tar.gz -> ocaml-ipaddr-${PV}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="=dev-ml/macaddr-${PV}:=
	dev-ml/stdlib-shims:=
	dev-ml/domain-name:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-ipaddr-${PV}"
