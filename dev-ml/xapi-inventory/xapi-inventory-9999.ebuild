# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 dune

DESCRIPTION="The XCP inventory library"
HOMEPAGE="https://github.com/xapi-project/xcp-inventory/"

EGIT_REPO_URI="https://github.com/xapi-project/xcp-inventory.git"
EGIT_BRANCH="master"

LICENSE="LGPL-2.1"
SLOT="0/${PV}"
IUSE="ocamlopt"

RDEPEND="dev-ml/base:=
	dev-ml/astring:=
	dev-ml/xapi-stdext-unix:=
	dev-ml/xapi-stdext-threads:=
	dev-ml/cmdliner:=
	dev-ml/uuidm:="
DEPEND="${RDEPEND}"
