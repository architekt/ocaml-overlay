# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 dune

DESCRIPTION="OCaml RRD library for use with XCP"
HOMEPAGE="https://github.com/xapi-project/xcp-rrd/"

EGIT_REPO_URI="https://github.com/xapi-project/xcp-rrd.git"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/base:=
	dev-ml/ppx_deriving_rpc:=
	dev-ml/rpclib:=
	dev-ml/xmlm:=
	dev-ml/uuidm:=
	dev-ml/ezjsonm:="
DEPEND="${RDEPEND}"

dune_src_compile() {
	dune build @install --profile=release || die
}
