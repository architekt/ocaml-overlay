# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Filesystem and block libraries"
HOMEPAGE="https://github.com/mirage/mirage-fs/"
SRC_URI="https://github.com/mirage/mirage-fs/archive/v3.0.1.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt test"
RESTRICT="!test? ( test )"

RDEPEND="dev-ml/fmt:=
	dev-ml/mirage-device:=
	dev-ml/lwt:=
	dev-ml/cstruct:=
	dev-ml/mirage-kv:="
DEPEND="${RDEPEND}"

dune_src_compile() {
	dune build --profile=release @install || die
}
