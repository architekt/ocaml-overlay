# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 dune

DESCRIPTION="Interface descriptions for services running on an XCP host"
HOMEPAGE="https://github.com/xapi-project/xcp-idl/"

EGIT_REPO_URI="https://github.com/xapi-project/xcp-idl.git"
EGIT_BRANCH="master"

LICENSE="LGPL-2.1"
SLOT="0/${PV}"
IUSE="ocamlopt"

RDEPEND="dev-ml/astring:=
	dev-ml/cmdliner:=
	dev-ml/cohttp:=
	dev-ml/message-switch-core:=
	dev-ml/message-switch-unix:=
	dev-ml/mtime:=
	dev-ml/ocaml-migrate-parsetree:=
	dev-ml/ppx_deriving_rpc:=
	dev-ml/ppx_sexp_conv:=
	dev-ml/re:=
	dev-ml/xapi-rrd:=
	dev-ml/sexplib:=
	dev-ml/base:=
	dev-ml/uri:=
	dev-ml/xapi-backtrace:=
	dev-ml/xapi-stdext-date:=
	dev-ml/xapi-stdext-pervasives:=
	dev-ml/xapi-stdext-threads:=
	dev-ml/xapi-inventory:=
	dev-ml/xmlm:="
DEPEND="${RDEPEND}"
