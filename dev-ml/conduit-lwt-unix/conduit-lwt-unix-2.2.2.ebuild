# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Dereference URIs into communication channels for Async or Lwt"
HOMEPAGE="https://github.com/mirage/ocaml-conduit/"
SRC_URI="https://github.com/mirage/ocaml-conduit/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/base:=
	dev-ml/ppx_sexp_conv:=
	=dev-ml/conduit-lwt-${PV}:=
	dev-ml/lwt:=
	dev-ml/uri:=
	dev-ml/ipaddr:=
	dev-ml/ipaddr-sexp:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-conduit-${PV}"

dune_src_compile() {
        dune build -p conduit-lwt-unix -j1 || die
}
