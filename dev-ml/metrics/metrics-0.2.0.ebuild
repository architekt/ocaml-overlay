# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Metrics infrastructure for OCaml"
HOMEPAGE="https://github.com/mirage/metrics/"
SRC_URI="https://github.com/mirage/metrics/archive/0.2.0.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/fmt:="
DEPEND="${RDEPEND}"

