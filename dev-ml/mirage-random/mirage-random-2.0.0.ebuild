# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Random-related devices for MirageOS"
HOMEPAGE="https://github.com/mirage/mirage-random/"
SRC_URI="https://github.com/mirage/mirage-random/archive/v2.0.0.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/cstruct:="
DEPEND="${RDEPEND}"

