# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

#DUNE_PKG_NAME="mirage-runtime"

inherit dune

DESCRIPTION="The base runtime library"
HOMEPAGE="https://github.com/mirage/mirage/"
SRC_URI="https://github.com/mirage/mirage/archive/v3.8.0.tar.gz -> mirage-${PV}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/ipaddr:=
	dev-ml/functoria-runtime:=
	dev-ml/fmt:=
	dev-ml/logs:=
	dev-ml/lwt:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/mirage-3.8.0"

#dune_src_compile() {
#	dune build -p mirage-runtime -j1 || die
#}
