# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Citrix's (deprecated) extensions to the ocaml standard library - Unix module extensions"
HOMEPAGE="https://github.com/xapi-project/stdext/"
SRC_URI="https://github.com/xapi-project/stdext/archive/v${PV}.tar.gz -> xen-api-${PV}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/base:=
	dev-ml/fd-send-recv:=
	dev-ml/xapi-stdext-pervasives:=
	dev-ml/xapi-stdext-std:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/stdext-${PV}"
