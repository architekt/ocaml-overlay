# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 dune

DESCRIPTION="extensible Markdown library and tool in 'pure OCaml'"
HOMEPAGE="https://github.com/ocaml/omd/"

EGIT_REPO_URI="https://github.com/ocaml/${PN}.git"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
IUSE="ocamlopt"

RDEPEND="dev-lang/ocaml:=[ocamlopt?]"
DEPEND="${RDEPEND}"

PATCHES=( "${FILESDIR}"/downgrade-dune-requirements-to-dune-2.0.patch )
