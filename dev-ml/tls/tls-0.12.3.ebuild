# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="TLS in pure OCaml"
HOMEPAGE="https://github.com/mirleft/ocaml-tls/"
SRC_URI="https://github.com/mirleft/ocaml-tls/archive/v0.12.3.tar.gz -> ocaml-tls-${PV}.tar.gz"

LICENSE="BSD"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/ppx_sexp_conv:=
	dev-ml/ppx_cstruct:=
	dev-ml/cstruct:=
	dev-ml/cstruct-sexp:=
	dev-ml/sexplib:=
	dev-ml/mirage-crypto:=
	dev-ml/mirage-crypto-pk:=
	dev-ml/mirage-crypto-rng:=
	dev-ml/x509:=
	dev-ml/domain-name:=
	dev-ml/fmt:=
	dev-ml/lwt:=
	dev-ml/ptime:=
	dev-ml/hacl_x25519:=
	dev-ml/fiat-p256:=
	dev-ml/hkdf:=
	dev-ml/logs:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-tls-${PV}"
