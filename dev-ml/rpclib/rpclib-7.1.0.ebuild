# Copyright 1999-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="A library to deal with RPCs in OCaml"
HOMEPAGE="https://github.com/mirage/ocaml-rpc"
SRC_URI="https://github.com/mirage/ocaml-rpc/archive/v7.1.0.tar.gz -> ocaml-rpc-${PV}.tar.gz"

LICENSE="LGPL-2.1-with-linking-exception"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/base64:=
	dev-ml/cmdliner:=
	dev-ml/rresult:=
	dev-ml/xmlm:=
	dev-ml/yojson:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-rpc-${PV}"
