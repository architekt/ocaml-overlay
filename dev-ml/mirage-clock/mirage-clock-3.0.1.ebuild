# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Portable clock implementation for Unix and Xen"
HOMEPAGE="https://github.com/mirage/mirage-clock/"
SRC_URI="https://github.com/mirage/mirage-clock/archive/v3.0.1.tar.gz -> mirage-clock-${PV}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND=""
DEPEND="${RDEPEND}"

