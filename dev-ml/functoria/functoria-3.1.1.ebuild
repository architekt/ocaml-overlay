# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="A DSL to invoke otherworldly functors"
HOMEPAGE="https://github.com/mirage/functoria/"
SRC_URI="https://github.com/mirage/functoria/archive/v3.1.1.tar.gz -> functoria-${PV}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/base:=
	dev-ml/cmdliner:=
	dev-ml/rresult:=
	dev-ml/astring:=
	dev-ml/fmt:=
	dev-ml/ocamlgraph:=
	dev-ml/logs:=
	dev-ml/bos:=
	dev-ml/fpath:=
	dev-ml/ptime:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/functoria-${PV}"
