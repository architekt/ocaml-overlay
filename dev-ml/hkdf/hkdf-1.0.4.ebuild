# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="HMAC-based Extract-and-Expand Key Derivation Function (HKDF) (RFC 5869)"
HOMEPAGE="https://github.com/hannesm/ocaml-hkdf/"
SRC_URI="https://github.com/hannesm/ocaml-hkdf/archive/v1.0.4.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/cstruct:=
	dev-ml/mirage-crypto:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-hkdf-1.0.4"
