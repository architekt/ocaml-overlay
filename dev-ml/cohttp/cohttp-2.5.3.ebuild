# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DUNE_PKG_NAME="cohttp"

inherit dune


DESCRIPTION="An OCaml library for HTTP clients and servers using Lwt or Async"
HOMEPAGE="https://github.com/mirage/ocaml-cohttp/"
SRC_URI="https://github.com/mirage/ocaml-cohttp/archive/v2.5.3.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-lang/ocaml:=[ocamlopt?]"
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-${PN}-${PV}"

dune_src_compile() {
        dune build -p cohttp -j1 || die
}
