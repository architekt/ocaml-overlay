# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DUNE_PKG_NAME="${PN}"

inherit dune2

MY_PN="ocaml-cohttp"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="An OCaml library for HTTP clients and servers"
HOMEPAGE="https://github.com/mirage/ocaml-cohttp/"
SRC_URI="https://github.com/mirage/${MY_PN}/archive/v${PV}.tar.gz -> ${MY_P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="doc ocamlopt"

RDEPEND="" ## TODO FROM SCRATCH
DEPEND="${RDEPEND}"

S="${WORKDIR}/${MY_P}"
