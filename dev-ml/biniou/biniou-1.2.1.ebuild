# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="A binary data serialization format inspired by JSON for OCaml"
HOMEPAGE="https://github.com/ocaml-community/biniou/"
SRC_URI="https://github.com/ocaml-community/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

SLOT="0/${PV}"
LICENSE="BSD"
KEYWORDS="~amd64"
IUSE="ocamlopt"

RDEPEND=">=dev-lang/ocaml-3.11:=[ocamlopt?]
	dev-ml/easy-format:=[ocamlopt?]"
DEPEND="${RDEPEND}"
