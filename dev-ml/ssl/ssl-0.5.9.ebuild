# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="OCaml SSL bindings"
HOMEPAGE="https://github.com/savonet/ocaml-ssl/"
SRC_URI="https://github.com/savonet/ocaml-${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-lang/ocaml:=[ocamlopt?]"
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-${PN}-${PV}"
