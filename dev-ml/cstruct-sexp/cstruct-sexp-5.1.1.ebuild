# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="S-expression serialisers for C-like structures"
HOMEPAGE="https://github.com/mirage/ocaml-cstruct/"
SRC_URI="https://github.com/mirage/ocaml-cstruct/archive/v${PV}.tar.gz -> ocaml-cstruct-${PV}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/sexplib:=
	=dev-ml/cstruct-${PV}:="
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-cstruct-${PV}"
