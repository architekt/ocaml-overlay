# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="An implementation of channels using page-aligned memory"
HOMEPAGE="https://github.com/mirage/mirage-channel/"
SRC_URI="https://github.com/mirage/mirage-channel/archive/v4.0.1.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/mirage-flow:=
	dev-ml/lwt:=
	dev-ml/cstruct:=
	dev-ml/logs:="
DEPEND="${RDEPEND}"

