# Copyright 2019-2020 Moteel Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Pure OCaml regular expressions, with support for Perl and POSIX-style strings"
HOMEPAGE="https://github.com/ocaml/ocaml-re/"
SRC_URI="https://github.com/ocaml/ocaml-${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2-with-linking-exception"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND=""
DEPEND="${RDEPEND}"

S="${WORKDIR}/ocaml-${PN}-${PV}"

