# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Citrix's (deprecated) extensions to the ocaml standard library"
HOMEPAGE="https://github.com/xapi-project/stdext/"
SRC_URI="https://github.com/xapi-project/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/uuidm:="
DEPEND="${RDEPEND}"

#S="${WORKDIR}/-${PV}"
