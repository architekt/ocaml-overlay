# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Helper library for gathering system configuration"
HOMEPAGE="https://github.com/ocaml/dune"
SRC_URI="https://github.com/ocaml/dune/archive/${PV}.tar.gz -> dune-${PV}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0/${PV}"
KEYWORDS="amd64 arm arm64 x86"
IUSE="ocamlopt"

DEPEND="dev-lang/ocaml:=[ocamlopt?]
	=dev-ml/dune-${PV}"
RDEPEND="${DEPEND}"

S="${WORKDIR}/dune-${PV}"

src_configure() {
	ocaml configure.ml --libdir "${EPREFIX}/usr/$(get_libdir)/ocaml" || die
}

src_install() {
	dune-install dune-configurator

	# Move docs to the appropriate place.
	if [ -d "${ED%/}/usr/doc/${PN}" ] ; then
		mkdir -p "${ED%/}/usr/share/doc/${PF}/" || die
		mv "${ED%/}/usr/doc/${PN}/"* "${ED%/}/usr/share/doc/${PF}/" || die
		rm -rf "${ED%/}/usr/doc" || die
	fi
}
