# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DUNE_PKG_NAME="menhirLib menhirSdk menhir coq-menhirlib"

inherit dune

DESCRIPTION="An LR(1) parser generator for OCaml"
HOMEPAGE="https://gitlab.inria.fr/fpottier/menhir/"
SRC_URI="https://gitlab.inria.fr/fpottier/${PN}/-/archive/${PV}/${PN}-${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-lang/ocaml:=[ocamlopt?]"
DEPEND="${RDEPEND}"

dune_src_install() {
	for i in $DUNE_PKG_NAME; do
		local pkg="$i"
		dune-install "${pkg}"
	done

	# Move docs to the appropriate place.
	if [ -d "${ED%/}/usr/doc/${pkg}" ] ; then
		mkdir -p "${ED%/}/usr/share/doc/${PF}/" || die
		mv "${ED%/}/usr/doc/${pkg}/"* "${ED%/}/usr/share/doc/${PF}/" || die
		rm -rf "${ED%/}/usr/doc" || die
	fi
	mv "${D}"/usr/man "${D}"/usr/share/man
}
