# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Xen-style shared memory rings"
HOMEPAGE="https://github.com/mirage/shared-memory-ring/"
SRC_URI="https://github.com/mirage/shared-memory-ring/archive/v3.1.0.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/cstruct:=
	dev-ml/ppx_cstruct:=
	dev-ml/mirage-profile:="
DEPEND="${RDEPEND}"

