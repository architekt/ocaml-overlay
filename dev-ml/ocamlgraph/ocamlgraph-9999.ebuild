# Copyright 2019-2020 Architekt Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 dune

DESCRIPTION="OCaml graph library"
HOMEPAGE="https://github.com/backtracking/ocamlgraph/"

EGIT_REPO_URI="https://github.com/backtracking/ocamlgraph.git"
EGIT_BRANCH="master"

LICENSE="LGPL-2.1"
SLOT="0"
IUSE="ocamlopt"

RDEPEND="dev-ml/stdlib-shims:="
DEPEND="${RDEPEND}"

