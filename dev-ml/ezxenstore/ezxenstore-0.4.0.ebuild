# Copyright 2019-2020 Moteel Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

#DUNE_PKG_NAME="${PN}"

inherit dune

#MY_PN=""
#MY_P="${MY_PN}-${PV}"

DESCRIPTION="An easy-to-use interface to xenstore"
HOMEPAGE="https://github.com/xapi-project/ezxenstore"
SRC_URI="https://github.com/xapi-project/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="doc ocamlopt"

RDEPEND="" ## TODO FROM SCRATCH
DEPEND="${RDEPEND}"

#S="${WORKDIR}/${MY_P}"
