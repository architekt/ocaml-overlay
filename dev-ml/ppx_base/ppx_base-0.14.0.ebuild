# Copyright 2019-2020 Moteel Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit dune

DESCRIPTION="Base set of ppx rewriters"
HOMEPAGE="https://github.com/janestreet/ppx_base/"
SRC_URI="https://github.com/janestreet/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0/${PV}"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="ocamlopt"

RDEPEND="dev-ml/ppx_cold:=
	dev-ml/ppx_compare:=
	dev-ml/ppx_enumerate:=
	dev-ml/ppx_hash:=
	dev-ml/ppx_js_style:=
	dev-ml/ppx_sexp_conv:=
	dev-ml/ppxlib:="
DEPEND="${RDEPEND}"
